AWSTemplateFormatVersion: "2010-09-09"
Description: Regional lambda/functions for GDH backend services
Parameters:
  EnvType:
    Description: Environment type
    Default: Dev
    Type: String
    AllowedValues:
      - Dev
      - Stage
      - Prod
    ConstraintDescription: must specify dev, stage or prod.
  DBUserPwd:
    Description: Password for the non-admin database user
    Type: String
Resources:
  GDHFunction:
    Type: "AWS::Lambda::Function"
    Properties:
      Description: "Main API function"
      Code:
        S3Bucket: !ImportValue "GDHCodeBucket"
        S3Key: !Join [ "/", [ "api-geodatahub-dk", !Ref EnvType, "api.zip"] ]
      FunctionName: "gdhfunction"
      Handler: "bin/main"
      MemorySize: 128
      Role: !ImportValue "GDHAPIRoleARN"
      Runtime: "go1.x"
      Timeout: 10
      VpcConfig:
        SubnetIds:
          - !ImportValue "GDHSubnetPrivateVpcId"
        SecurityGroupIds:
          - !ImportValue "GDHSecurityGroupLambdaId"
          - !ImportValue "GDHSecurityGroupRDSId"
      Environment:
       Variables:
        DBHOST:
          Fn::ImportValue: !Sub 'GDH${EnvType}DBEndpoint'
        DBUSR: "geodatahub"
        DBNONADMPASSWORD: !Ref DBUserPwd
        DBTABLE: "geodatahub"
        DO_LAMBDA: "1"

  GDHDBSetupFunction:
    Type: "AWS::Lambda::Function"
    Properties:
      Description: "Function to setup the initial database"
      Code:
        S3Bucket: !ImportValue "GDHCodeBucket"
        S3Key: !Join [ "/", [ "api-geodatahub-dk", !Ref EnvType, "init_database.zip"] ]
      FunctionName: "gdhdbsetupfunction"
      Handler: "bin/init_database"
      MemorySize: 128
      Role: !ImportValue "GDHAPIRoleARN"
      Runtime: "go1.x"
      Timeout: 20
      VpcConfig:
        SubnetIds:
          - !ImportValue "GDHSubnetPrivateVpcId"
        SecurityGroupIds:
          - !ImportValue "GDHSecurityGroupLambdaId"
          - !ImportValue "GDHSecurityGroupRDSId"
      Environment:
       Variables:
        DBHOST:
          Fn::ImportValue: !Sub "GDH${EnvType}DBEndpoint"
        DBUSR: "root"
        DBNONADMUSR: "geodatahub"
        DBNONADMPASSWORD: !Ref DBUserPwd
        DBPASSWORD: "SecretPassword"
        DBTABLE: "geodatahub"

  GDHDBMigrationFunction:
    Type: "AWS::Lambda::Function"
    Properties:
      Description: "Function to migrate the database schema"
      Code:
        S3Bucket: !ImportValue "GDHCodeBucket"
        S3Key: !Join [ "/", [ "api-geodatahub-dk", !Ref EnvType, "migrate_database.zip"] ]
      FunctionName: "gdhdbmigtatefunction"
      Handler: "bin/migrate"
      MemorySize: 128
      Role: !ImportValue "GDHAPIRoleARN"
      Runtime: "go1.x"
      Timeout: 20
      VpcConfig:
        SubnetIds:
          - !ImportValue "GDHSubnetPrivateVpcId"
        SecurityGroupIds:
          - !ImportValue "GDHSecurityGroupRDSId"
          - !ImportValue "GDHSecurityGroupLambdaId"
      Environment:
       Variables:
        DBHOST:
                    Fn::ImportValue: !Sub "GDH${EnvType}DBEndpoint"
        DBNONADMUSR: "geodatahub"
        DBNONADMPASSWORD: !Ref DBUserPwd
        DBTABLE: "geodatahub"

#  GDHFunctionLogGroup:
#    Type: "AWS::Logs::LogGroup"
#    Properties:
#      LogGroupName: !Sub "/aws/lambda/gdhfunction"
#      RetentionInDays: 90

Outputs:
  GDHAPIFunctionARN:
    Description: "ARN to describe the API lambda function"
    Value: !GetAtt "GDHFunction.Arn"
    Export:
      Name: "GDHAPIFunctionARN"

  GDHAPIFunction:
    Description: "Name to describe the API lambda function"
    Value: !Ref GDHFunction
    Export:
      Name: "GDHAPIFunction"
