# GeoDataHub DevOps Repository

**This repository is part of a larger experiment to build a geoscientific data catalog using modern cloud architectures with a special focus on collecting all data types within a single system to break down silos.**

**The project is stable but not production-ready and no longer maintained in its current form. It's released to inspire others and showcase how to build such a platform. See [getgeodata](getgeodata.com) for more.**

**If you have technical questions about the design (not the installation or setup) you are welcome to contact hello@getgeodata.com**

This repository contains CloudFormation (cfn) templates for the AWS intrastructure.

# Deploy development environment

First create the required parameters if using a new datacenter. The development environment is currently located in the eu-west-1 (Ireland) datacenter. Run,

```bash
./deploy-dev.sh
```

To setup the infrastructure. This will build all the required components. If everything is already running this command will do nothing.


# Add parameters to cloud formation

To create a new password for development databases use the following command,

```bash
aws --region eu-west-1 ssm put-parameter --name GDHDevDatabasePassword --type SecureString --value "YourNewPassword"
```

Please remember to also store any password in pass!

# Deploying global resources

Global resources are only supposed to be deployed in one region (eu-central-1, Frankfurt). Global resources should only be created once and reused between regions.


To create IAM roles in all regions run,

```bash
aws --region eu-central-1 cloudformation create-stack  --stack-name gdh-global-roles  --template-body file://gdh-global-roles.yaml --capabilities CAPABILITY_NAMED_IAM
```

# Reference

For more information read the [DevOps wiki page](https://gitlab.com/GeoDataHub/GeoDataHub/wikis/aws/DevOps).
