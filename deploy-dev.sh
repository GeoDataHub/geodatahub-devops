#!/bin/bash

if [ -z $1 ];
   then
       ENV="Dev"
else
    ENV=$1
fi

if [ -z $2 ];
then
    SKIPUPLOAD=true
else
    SKIPUPLOAD=false
fi

echo "Running environment $ENV"
if [ $ENV == "Dev" ]; then
    REGION="eu-west-1" # Dev region is always Ireland
    DBRootPass=$(aws --profile $ENV ssm get-parameter --name GDHDevDatabasePassword --with-decrypt)
else
    REGION="eu-central-1" # Prod region is always Frankfurt
    DBRootPass=$(aws --profile $ENV ssm get-parameter --name GDHProdDatabasePassword --with-decrypt)
fi
echo "Deploying $ENV environment in $REGION"

#DBUserPass=$(aws --region eu-west-1 ssm get-parameter --name GDHDatabaseDevUserPassword --with-decrypt)
STACK_ORDER=(gdh-regional-networking gdh-regional-roles \
	                                   gdh-regional-s3 build-api gdh-regional-auth \
    	                               gdh-regional-storage gdh-regional-functions \
	                                   gdh-regional-api
	    )

create-stack ()
{
    if [[ $1 == *"roles" || $1 == *"auth" ]];
       then
	   aws --profile $ENV cloudformation create-stack \
	   --stack-name $1-${ENV}  --template-body file://${1}.yaml \
	   --parameters ParameterKey=EnvType,ParameterValue=${ENV} \
	   --capabilities CAPABILITY_IAM
    elif [[ $1 == *"functions" ]];
    then
	aws --profile $ENV cloudformation create-stack  --stack-name \
	$1-${ENV}  --template-body file://${1}.yaml --parameters \
	ParameterKey=EnvType,ParameterValue=${ENV} \
	ParameterKey=DBUserPwd,ParameterValue=${DBUserPass} \
    --capabilities CAPABILITY_AUTO_EXPAND
    else
	aws --profile $ENV cloudformation create-stack  --stack-name \
	$1-${ENV}  --template-body file://${1}.yaml --parameters \
	ParameterKey=EnvType,ParameterValue=${ENV} \
    --capabilities CAPABILITY_AUTO_EXPAND
    fi

    deploy-stack-finished $1
}

delete-stack ()
{
    echo "Deleting $1"
    aws --profile $ENV cloudformation delete-stack  --stack-name $1-${ENV}
    DELETE_RESPONSE=$(aws --profile $ENV cloudformation describe-stacks  --stack-name $1-${ENV} 2>&1)
    printf "Waiting"
    while [[ ! "$DELETE_RESPONSE" == *"does not exist"* ]]
    do
	printf "."
	sleep 5
	DELETE_RESPONSE=$(aws --profile $ENV cloudformation describe-stacks  --stack-name $1-${ENV} 2>&1)
    done
}

update-stack ()
{
    if [[ $1 == *"roles" || $1 == *"auth" ]];
    then
	RESPONSE=$(aws --profile $ENV cloudformation update-stack --stack-name $1-${ENV}  --template-body file://${1}.yaml --parameters ParameterKey=EnvType,ParameterValue=${ENV} --capabilities CAPABILITY_IAM 2>&1)
    elif [[ $1 == *"functions" ]];
	 then
       	 RESPONSE=$(aws --profile $ENV cloudformation update-stack --stack-name $1-${ENV}  --template-body file://${1}.yaml  --parameters ParameterKey=EnvType,ParameterValue=${ENV}  ParameterKey=DBUserPwd,ParameterValue=${DBUserPass} 2>&1)
    else
	RESPONSE=$(aws --profile $ENV cloudformation update-stack --stack-name $1-${ENV} --template-body file://${1}.yaml  --parameters ParameterKey=EnvType,ParameterValue=${ENV} 2>&1)
    fi
    VAL=$?

    if [[ "$RESPONSE" == *"No updates are to be performed."* ]]; then
	# Template is already up to date
	echo "No updates"
	return 0
    elif [[ "$RESPONSE" == *"does not exist"* ]]; then
	# No template exists in AWS
	return 1
    elif [[ "$RESPONSE" == *"ROLLBACK_COMPLETE state and can not be updated"* ]]; then
	while true; do
	    read -p "Stack $1 looks to be broken. Would you like to redeploy? THIS WILL DELETE ALL EXISTING INFRASTRUCTURE! [y/n] " yn
	    case $yn in
		[Yy]* ) delete-stack $1; create-stack $1; return 0; break;;
		[Nn]* ) exit;;
		* ) echo "Please answer yes or no.";;
	    esac
	done
    fi

    echo $RESPONSE
    echo "Unknown error message. Please fix error manually"
    exit
}

# Check if stack is fully deployed
deploy-stack-finished()
{
    printf "Creating stack"
    TEMPLATE_STATE=$(aws --profile $ENV cloudformation describe-stacks  --stack-name $1-${ENV} | grep 'StackStatus' 2>&1)
    while [[ ! "$(aws --profile $ENV cloudformation describe-stacks  --stack-name $1-${ENV} | grep 'StackStatus')" == *"CREATE_COMPLETE"* ]]
    do
	printf "."
	if [[ "$TEMPLATE_STATE" == *"ROLLBACK"* ]]; then
	    echo "Error creating $1. Unable to continue!"
	    exit
	else
	    sleep 10
	    TEMPLATE_STATE=$(aws --profile $ENV cloudformation describe-stacks  --stack-name $1-${ENV} | grep 'StackStatus' 2>&1)
	fi
    done
    
}


build-api-v2()
{
    SCRIPTDIR=$PWD
    cd /home/christian/go/src/gitlab.com/api.geodatahub.dk

    # Build -> Package binaries
    make package

    # Deploy binaries
    if [[ $ENV == *"Prod"* ]]; then
    aws --profile $ENV s3 sync bin/ s3://geodatahub-api-prod-${REGION}/api-geodatahub-dk/${ENV}/ --exclude "*" --include "*.zip"
    else
    aws --profile $ENV s3 sync bin/ s3://geodatahub-api-dev-${REGION}/api-geodatahub-dk/${ENV}/ --exclude "*" --include "*.zip"
    fi

    cd $SCRIPTDIR
}

build-api()
{
    # Build the dev api from scratch
    mkdir .artifacts
    cd .artifacts
#    CLONE_RESP=$(git clone git@gitlab.com:GeoDataHub/GeoDataHub-API.git api.geodatahub.dk 2>1&)
    cd api.geodatahub.dk
    CHECKOUT_RESP=$(git checkout $ENV 2>1&)
    if [[ ! "$CHECKOUT_RESP" == *"up-to-date"* ]] || [[ ! "CLONE_RESP" == *"not an empty"* ]]; then
	# Skip if no new updates
	make
	zip -r api-geodatahub-dk.zip bin
	aws --profile $ENV s3 cp api-geodatahub-dk.zip s3://geodatahub-api-dev-${REGION}/api-geodatahub-dk/prod/api-geodatahub-dk.zip
    fi
    cd ..
    cd ..
}

# Create the initial database by
# calling the lambda functions that creates
# the database schema
init-database()
{
    aws lambda invoke --function-name "gdhdbsetupfunction" setupdb.log 
    aws lambda invoke --function-name "gdhdbmigtatefunction" migratedb.log
}

# Loop all stacks
for template in ${STACK_ORDER[@]}; do

    if [[ $template == "build-api" ]];
    then
	if ! $SKIPUPLOAD;
	then
	    build-api-v2
	else
	    echo "Skipping building binaries"
	fi
    elif [[ $template == "init_database" ]];
    then
	     init-database
    else
	echo "Updating template $template"
	update-stack $template
	UPDATE_STATUS="$?"

	# Error code: 1 - No updates to perform
	if [ "$UPDATE_STATUS" -eq "1" ];
	then
	    
	    while true; do
		read -p "Template does not exist! Create it? [y/n] " yn
		case $yn in
		    [Yy]* ) create-stack $template; deploy-stack-finished $template; break;;
		    [Nn]* ) exit;;
		    * ) echo "Please answer yes or no.";;
		esac
	    done
	    echo "DONE"
	fi
    fi
done
