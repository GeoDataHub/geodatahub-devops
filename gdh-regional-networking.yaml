AWSTemplateFormatVersion: "2010-09-09"
Description: Regional infrastructure for GDH backend services
Parameters:
  EnvType:
    Description: Environment type
    Default: Dev
    Type: String
    AllowedValues:
      - Dev
      - Stage
      - Prod
    ConstraintDescription: must specify Dev, Stage or Prod.
Resources:
#  GDHTopLevelStack:
#    Type: "AWS::CloudFormation::Stack"
#    Properties:
#      Tags:
#        -
#           Key: "stage"
#           Value: "dev"
#        -
#           Key: "product"
#           Value: "gdh"

  GDHVPC:
    Type: "AWS::EC2::VPC"
    Properties:
      CidrBlock: 10.0.0.0/16

  GDHInetGateway:
    Type: "AWS::EC2::InternetGateway"

  GDHInetGatewayAttachment:
    Type: "AWS::EC2::VPCGatewayAttachment"
    DependsOn: GDHInetGateway
    Properties:
      VpcId:
        Ref: GDHVPC
      InternetGatewayId:
        Ref: GDHInetGateway

  # Private subspace
  GDHSubnetPrivate:
    Type: "AWS::EC2::Subnet"
    Properties:
      VpcId:
        Ref: GDHVPC
      AvailabilityZone: !Join [ '', [!Ref "AWS::Region", "a"] ]
      CidrBlock: 10.0.1.0/24

  # Setup public subnet
  GDHSubnetPublic:
    Type: "AWS::EC2::Subnet"
    Properties:
      VpcId:
        Ref: GDHVPC
      AvailabilityZone: !Join [ '', [!Ref "AWS::Region", "b"] ]
      CidrBlock: 10.0.2.0/24

  GDHRouteTablePublic:
    Type: "AWS::EC2::RouteTable"
    Properties:
      VpcId:
        Ref: GDHVPC

  GDHRouteInetGateway:
    Type: "AWS::EC2::Route"
    DependsOn: GDHInetGatewayAttachment
    Properties:
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId:
        Ref: GDHInetGateway
      RouteTableId:
        Ref: GDHRouteTablePublic

  GDHSubnetRouteTablePublic:
    Type: "AWS::EC2::SubnetRouteTableAssociation"
    Properties:
      RouteTableId:
        Ref: GDHRouteTablePublic
      SubnetId:
        Ref: GDHSubnetPublic      

  GDHSecurityGroupRDS:
    DependsOn: GDHSecurityGroupLambda
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: Security Group to hold RDS instances
      VpcId:
        Ref: GDHVPC
      # TODO - The following code will allow Lambda automatic access to
      # RDS but needs a full redeploy
      SecurityGroupIngress:
      - SourceSecurityGroupId: !GetAtt "GDHSecurityGroupLambda.GroupId"
        IpProtocol: TCP
        FromPort: 5432
        ToPort: 5432

  GDHSecurityGroupLambda:
    DependsOn: GDHVPC
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: Security group to hold Lambda functions
      VpcId:
        Ref: GDHVPC

  GDHDBSubnetGroup:
    Type: "AWS::RDS::DBSubnetGroup"
    Properties:
      DBSubnetGroupDescription: "Group for RDS instances for GDH"
      DBSubnetGroupName: GDHDBSubnetGroup
      SubnetIds:
        # There must always be two
        # subnets for the RDS
        # but the security group
        # disallows inet access
        - Ref: GDHSubnetPrivate
        - Ref: GDHSubnetPublic

Outputs:
  GDHSubnetPrivateVpcId:
    Description: "VPC ID for the private subnet"
    Value: !Ref GDHSubnetPrivate
    Export:
      Name: "GDHSubnetPrivateVpcId"

  GDHSecurityGroupRDSId:
    Description: "ID for the security group RDS"
    Value: !GetAtt "GDHSecurityGroupRDS.GroupId"
    Export:
      Name: "GDHSecurityGroupRDSId"

  GDHSecurityGroupLambdaId:
    Description: "ID for the security group Lambda"
    Value: !GetAtt "GDHSecurityGroupLambda.GroupId"
    Export:
      Name: "GDHSecurityGroupLambdaId"

  GDHDBSubnetGroup:
    Description: "Subnet reference for the RDS database"
    Value: !Ref "GDHDBSubnetGroup"
    Export:
      Name: "GDHDBSubnetGroup"
