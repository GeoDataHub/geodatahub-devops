
deploy-gdh-ireland-networking:
	aws --region eu-west-1 cloudformation update-stack  --stack-name gdh-serverless-backend  --template-body file://gdh-regional-networking.yaml --parameters ParameterKey=EnvType,ParameterValue=${ENV}

create-gdh-stockholm-networking:
	aws --region eu-north-1 cloudformation create-stack  --stack-name gdh-serverless-networking  --template-body file://gdh-regional-networking.yaml --parameters ParameterKey=EnvType,ParameterValue=${ENV}

deploy-gdh-stockholm-networking:
	aws --region eu-north-1 cloudformation update-stack  --stack-name gdh-serverless-networking  --template-body file://gdh-regional-networking.yaml --parameters ParameterKey=EnvType,ParameterValue=${ENV}


create-devel: create-gdh-stockholm-networking
	echo "Development environment created"

update-devel: deploy-gdh-stockholm-networking
	ENV="dev"
	echo "Development environment updated"



#
# CloudFormation Stack-set definitions
#
create-gdh-regional-networking:
	aws cloudformation create-stack-set --stack-set-name gdh-serverless-networking --template-body file://gdh-regional-networking.yaml

deploy-gdh-regional-networking:
        # eu-west-1: Ireland
	aws cloudformation create-stack-instances --stack-set-name gdh-serverless-networking --accounts ${AWS_ACCOUNT_ID} --regions "eu-west-1"
